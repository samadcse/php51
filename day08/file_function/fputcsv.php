<?php
$list = array
(
    "Peter,Griffin,Oslo,Norway",
    "Glenn,Quagmire,Oslo,Norway",
);

$file = fopen("contacts.csv","w");

foreach ($list as $line)
{
    fputcsv($file,explode(',',$line));
}

fclose($file); ?>

/*
The CSV file will look like this after the code above has been executed:

Peter,Griffin,Oslo,Norway
Glenn,Quagmire,Oslo,Norway
*/
